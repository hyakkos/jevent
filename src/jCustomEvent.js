import JEvent from "./jEvent"

/**
 * Class JCustomEvent.
 * @extends JEvent
 */
export default class JCustomEvent extends JEvent {

    /**
     * 自定义事件标题
     * @property message
     * @type String
     * @readonly
     */
    message = null

    /**
     * 自定义事件标题其他可能需要的数据
     * @property data
     * @type {Object}
     */
    data = null

    /**
     * 构造函数
     * @param type {string}
     * @param message {string} 可选
     * @param data {object} 可选
     * @param { EventInit } eventInit 可选
     */
    constructor(type, message = null, data = null, eventInit = { bubbles: false, cancelable: false, composed: false }) {

        super(type, eventInit)

        this.message = message || type

        this.data = data
    }

    /**
     *
     * @return {String}
     **/
    toString() {
        return `[JCustomEvent] type=${this.type} message=${this.message}`
    }
}