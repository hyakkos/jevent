import JCustomEvent from "./jCustomEvent"

export default class JErrorEvent extends JCustomEvent {
    /**
     * 错误事件
     * @param message {string} 可选
     * @param data {object} 可选
     * @param { EventInit } eventInit 可选
     */
    constructor(message = null, data = null, eventInit = { bubbles: false, cancelable: false, composed: false }) {
        super("JError", message, data, eventInit)
    }

     /**
     *
     * @return {String}
     **/
     toString() {
        return `[JErrorEvent] type=${this.type} message=${this.message}`
    }
}